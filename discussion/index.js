// EXPRESS SETUP
// 1. Import by using the 'require' directive to get access to the components of express package/dependencies
const { request, response } = require('express')
const express = require('express')
// 2. Use the express() function and assign in to an app variable to create an express app or app server
const app = express()
// 3. Declare a variable for the port of the server
const port = 3000

// Middlewares
// These two .use are essential in express
// Allows your app to read json format data
app.use(express.json())
// Allows your app to read data from forms
app.use(express.urlencoded({ extended: true }))

// Routes

// Get request route
app.get('/', (request, response) => {
    // once the route is accessed it will then send a string response containing "Hello World"
    response.send('Hello World!')
})

app.get('/hello', (request, response) => {
    response.send("Hello from /hello endpoint!")
})

// Register user route 

// mock database
let users = [];

app.post('/register', (request, response) => {
    if (request.body.username !== " " && request.body.password !== " ") {
        users.push(request.body)
        console.log(users)
        response.send(`User ${request.body.username} successfully registered`)
    } else {
        response.send('Please input BOTH username and password')
    }
})

// Put request route
app.put('/change-password', (request, response) => {
    let message

    for (let i = 0; i < users.length; i++) {
        if (request.body.username == users[i].username) {
            users[i].password == request.body.password
            message = `User ${request.body.username}'s password has been updated!`
            break
        } else {
            message = 'User does not exist.'
        }
    }
    response.send(message)
})

// Activity
// 1. GET route /home which will print out a simple message
app.get('/home', (request, response) => {
    response.send(`Welcome to homepage!`);
})


// 3. Create a GET route that will access the /users route that will retrieve all the users in the mock database
app.get('/users', (request, response) => {
    response.send(users);
})


// 5. Create a DELETE route that will access the /delete-user route to remove a user from the mock database
app.delete('/delete-user', (request, response) => {
    let message
    for (let i = 0; i < users.length; i++) {
        if (request.body.username == users[i].username) {
            users.splice(i, 1);
            message = `User ${request.body.username} has been deleted!`
            break
        } else {
            message = `User ${request.body.username} does not exist. Please double check your input.`
        }
    }
    response.send(message)
})

app.listen(port, () => console.log(`Server is running at port ${port}.`))